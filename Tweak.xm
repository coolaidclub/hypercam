#import <UIKit/UIKit.h>

NSBundle *bundle = [[[NSBundle alloc] initWithPath:@"/Library/MobileSubstrate/DynamicLibraries/hypercamBundle.bundle"] autorelease];

%ctor {
	NSAutoreleasePool *swag = [[NSAutoreleasePool alloc] init];
	[[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidFinishLaunchingNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *block) {
		CGRect frame = [UIScreen mainScreen].bounds;
		UIWindow *window = [[UIWindow alloc] initWithFrame:frame];
		window.windowLevel = 99996.0f;
		
		UIImage *image = [UIImage imageWithContentsOfFile:[bundle pathForResource:@"un" ofType:@"png"]];
		int newWidth = [UIScreen mainScreen].bounds.size.width / 2;
		int newHeight = image.size.height * newWidth / image.size.width;
		UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,newWidth,newHeight)];
		[imageView setImage:image];
		
		[window addSubview:imageView];
		window.userInteractionEnabled = NO;
		window.hidden = NO;
	}];
	[swag drain];//draining all ur swag
}
