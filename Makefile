THEOS_PACKAGE_DIR_NAME = debs
TARGET = iphone:clang:7.1:7.1
ARCHS = armv7 arm64

include theos/makefiles/common.mk

TWEAK_NAME = hypercam
hypercam_FILES = Tweak.xm
hypercam_FRAMEWORKS = UIKit QuartzCore

include $(THEOS_MAKE_PATH)/tweak.mk

after-install::
	install.exec "killall -9 SpringBoard"

BUNDLE_NAME = hypercamBundle
hypercamBundle_INSTALL_PATH = /Library/MobileSubstrate/DynamicLibraries
include $(THEOS_MAKE_PATH)/bundle.mk